<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = 'attachments';

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * 添付ファイルを持つ所有タスクの取得
     */
    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
