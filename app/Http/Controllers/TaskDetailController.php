<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use DB;
use App\Task;

class TaskDetailController extends Controller
{
    /**
     * タスクの詳細を表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function show_id(Request $request)
    {
        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        //URLパラメータからIDを取得
        $id = $request->id;

        //tasksテーブルからIDを用いてタスク情報を取得
        $items_task = DB::table('tasks')->where('id',$id)->get(['id','title','end_expected_date','achievement_flg','detail']);

        //attachmentsテーブルからIDを用いて添付ファイル情報を取得
        $items_attachment = DB::table('attachments')->where('task_id',$id)->get(['id','task_id','file_name']);

        return view('task_detail',['items_task' => $items_task],['items_attachment' => $items_attachment]);
    }



    /**
     * ファイルをダウンロードさせる機能
     *
     * @param  Request  $request
     * @return Response
     */
    public function file_download(Request $request)
    {
        //URLパラメータからIDを取得        
        $id = $request->id;

        //URLパラメータからファイル名を取得    
        $file_name = $request->file_name;

        //ファイルパスを取得
        $pathToFile = storage_path().'/app/public/'.$id.'/'.$file_name;

        //パスに存在するファイルをダウンロードさせる
        return response()->download($pathToFile);
    }

}
