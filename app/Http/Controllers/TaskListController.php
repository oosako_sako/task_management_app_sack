<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Task;
use App\Http\Requests\TaskRequest;
use Validator;

class TaskListController extends Controller
{
    /**
     * 初期表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        return view('task_list');
    }

    /**
     * DBに保存されているタスクをリスト表示(セッションに保持されている値で検索結果を出している状態)
     *
     * @param  Request  $request
     * @return Response
     */
    public function show(Request $request)
    {
        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        //バリデーションルール
        $rules = [
            'id'        => ['numeric','nullable'],
            'from_date' => ['nullable','regex:|\d{4}\/\d{1,2}\/\d{1,2}|'],
            'to_date'   => ['nullable','regex:|\d{4}\/\d{1,2}\/\d{1,2}|'],
        ];

        //バリデーションメッセージ
        $messages = [
            'id.numeric'      => 'idは半角数字で入力して下さい',
            'from_date.regex' => 'YYYY/MM/DDの形で入力して下さい(From側)',
            'to_date.regex'   => 'YYYY/MM/DDの形で入力して下さい(To側)',
        ];

        //バリデーター
        $validator = Validator::make($request->all(),$rules,$messages);

        if($validator->fails()){
            return redirect('/')
                            ->withErrors($validator)
                            ->withInput();
        }

        //動的クエリ生成
        $query = Task::query();

        //ID検索
        if(!empty(session('id'))){
            $query->where('id',session('id'));
        }

        //タイトル検索
        if(!empty(session('title'))){
            $query->where('title','like','%'.session('title').'%');
        }
        
        //達成予定日検索(From)
        if(!empty(session('from_date')) && empty(session('to_date'))){
            $query->where('end_expected_date','>=',session('from_date'));
        }

        //達成予定日検索(To)
        if(empty(session('from_date')) && !empty(session('to_date'))){
            $query->where('end_expected_date','<=',session('to_date'));
        }

        //達成予定日検索(From-To)
        if(!empty(session('from_date')) && !empty(session('to_date'))){
            $query->whereBetween('end_expected_date',[session('from_date'),session('to_date')]);
        }

        //添付ファイル有無："あり"選択時
        if(session('file') == '1'){
            $query->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('attachments')
                          ->whereRaw('attachments.task_id = tasks.id');
                });
        }

        //添付ファイル有無："なし"選択時
        if(session('file') == '2'){
            $query->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('attachments')
                          ->whereRaw('attachments.task_id = tasks.id');
                });
        }

        //達成の有無検索
        if(session('achivment') == ''){
            $query->where('achievement_flg','!=','1');
        }

        //ソート
        if(session('sort') != ''){
            $query->orderBy(session('sort'),session('asc_desc'));
        }

        //生成したクエリで検索
        $items = $query->get(['id','title','achievement_flg']);

        return view('task_list',['items' => $items]);
    }

    /**
     * DBに保存されているタスクを、POSTされた要素を用いて検索、リスト表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function search(Request $request)
    {

        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        //POSTされた要素をセッションに保存
        $request->session()->put('id', $request->id);
        $request->session()->put('title', $request->title);
        $request->session()->put('from_date', $request->from_date);
        $request->session()->put('to_date', $request->to_date);
        $request->session()->put('file', $request->file);
        $request->session()->put('achivment', $request->achivment);
        $request->session()->put('sort', $request->sort);
        $request->session()->put('asc_desc', $request->asc_desc);

        //バリデーションルール
        $rules = [
            'id'        => ['numeric','nullable'],
            'from_date' => ['nullable','regex:|\d{4}\/\d{1,2}\/\d{1,2}|'],
            'to_date'   => ['nullable','regex:|\d{4}\/\d{1,2}\/\d{1,2}|'],
        ];

        //バリデーションメッセージ
        $messages = [
            'id.numeric'      => 'idは半角数字で入力して下さい',
            'from_date.regex' => 'YYYY/MM/DDの形で入力して下さい(From側)',
            'to_date.regex'   => 'YYYY/MM/DDの形で入力して下さい(To側)',
        ];

        //バリデーター
        $validator = Validator::make($request->all(),$rules,$messages);

        if($validator->fails()){
            return redirect('/')
                            ->withErrors($validator)
                            ->withInput();
        }
        
        //動的クエリ生成
        $query = Task::query();

        //ID検索        
        if(!empty($request->id)){
            $query->where('id',$request->id);
        }

        //タイトル検索        
        if(!empty($request->title)){
            $query->where('title','like','%'.$request->title.'%');
        }

        //達成予定日検索(From)        
        if(!empty($request->from_date) && empty($request->to_date)){
            $query->where('end_expected_date','>=',$request->from_date);
        }

        //達成予定日検索(To)
        if(empty($request->from_date) && !empty($request->to_date)){
            $query->where('end_expected_date','<=',$request->to_date);
        }

        //達成予定日検索(From-To)
        if(!empty($request->from_date) && !empty($request->to_date)){
            $query->whereBetween('end_expected_date',[$request->from_date,$request->to_date]);
        }

        //添付ファイル有無："あり"選択時        
        if($request->file == '1'){
            $query->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('attachments')
                          ->whereRaw('attachments.task_id = tasks.id');
                });
        }

        //添付ファイル有無："なし"選択時
        if($request->file == '2'){
            $query->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('attachments')
                          ->whereRaw('attachments.task_id = tasks.id');
                });
        }

        //達成の有無検索        
        if($request->achivment == ''){
            $query->where('achievement_flg','!=','1');
        }

        //ソート        
        if($request->sort != ''){
            $query->orderBy($request->sort,$request->asc_desc);
        }

        //生成したクエリで検索
        $items = $query->get(['id','title','achievement_flg']);

        return view('task_list',['items' => $items]);
    }
}
