<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Task;
use Validator;
use Carbon\Carbon;

class TaskRegistrationController extends Controller
{
    /**
     * タスク一覧画面から新規登録ボタン押下で遷移された場合の初期表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function show(Request $request)
    {
        //セッションに保存されている値を選択除去
        $request->session()->forget('id_regist');
        $request->session()->forget('title_regist');
        $request->session()->forget('end_expected_date_regist');
        $request->session()->forget('achievement_flg_regist');
        $request->session()->forget('detail_regist');

        return view('task_registration');
    }

    /**
     * バリデーション後の遷移用表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function show_valid(Request $request)
    {
        return view('task_registration');
    }

    /**
     * タスク詳細画面からIDを指定して遷移された場合の初期表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function show_id(Request $request)
    {
        //URLパラメータからIDを取得
        $id = $request->id;

        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        //tasksテーブルからIDを用いてタスク情報を取得
        $items_task = DB::table('tasks')->where('id',$id)->get(['id','title','end_expected_date','achievement_flg','detail','updated_at']);

        //DBから取得したタスク情報をセッションに保存
        $request->session()->put('id_regist', $items_task[0]->id);
        $request->session()->put('title_regist', $items_task[0]->title);
        $request->session()->put('end_expected_date_regist', str_replace('-', '/', $items_task[0]->end_expected_date));
        $request->session()->put('achievement_flg_regist', $items_task[0]->achievement_flg);
        $request->session()->put('detail_regist', $items_task[0]->detail);
        $request->session()->put('updated_at', $items_task[0]->updated_at);

        //attachmentsテーブルからIDを用いてタスク情報を取得
        $items_attachment = DB::table('attachments')->where('task_id',$id)->get(['id','task_id','file_name']);

        return view('task_registration',['items_attachment' => $items_attachment],['id' => $id] );
    }

    /**
     * バリデーション後の遷移用表示
     *
     * @param  Request  $request
     * @return Response
     */
    public function show_id_valid(Request $request)
    {
        //URLパラメータからIDを取得
        $id = $request->id;

        //attachmentsテーブルからIDを用いてタスク情報を取得
        $items_attachment = DB::table('attachments')->where('task_id',$id)->get(['id','task_id','file_name']);

        return view('task_registration',['items_attachment' => $items_attachment],['id' => $id] );
    }

    /**
     * データの新規登録処理
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {

        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        //POSTされた要素をセッションに保存
        $request->session()->put('id_regist', $request->id);
        $request->session()->put('title_regist', $request->title);
        $request->session()->put('end_expected_date_regist', $request->date);
        $request->session()->put('achievement_flg_regist', $request->achivment);
        $request->session()->put('detail_regist', $request->detail);
        $request->session()->put('updated_at', $request->updated_at);

        //バリデーションルール
        $rules = [
            'title'        => ['required'],
            'date'         => ['required','regex:|\d{4}\/\d{1,2}\/\d{1,2}|'],
            'detail'       => ['required','max: 400'],
        ];

        //バリデーションメッセージ
        $messages = [
            'title.required'      => '必須入力です',
            'date.required'       => '必須入力です',
            'detail.required'     => '必須入力です',
            
            'date.regex' => 'YYYY/MM/DDの形で入力して下さい(From側)',
            'detail.max'   => '400文字まで入力できます',
        ];

        //バリデーター
        $validator = Validator::make($request->all(),$rules,$messages);

        if($validator->fails()){
            return redirect('/taskregistration/validation_error')
                            ->withErrors($validator)
                            ->withInput();
        }

        //POSTされた要素でDBに新規登録処理(タスク情報)
        $param_tasks = [
            'title' => $request->title,
            'end_expected_date' => $request->date,
            'achievement_flg' => $request->achivment,
            'detail' => $request->detail,
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),

        ];
        DB::table('tasks')->insert($param_tasks);

        //tasksテーブルに保存されているIDカラムの最大値を取得
        $max_id = Task::max('id');

        //POSTされた要素でDBに新規登録処理(添付ファイル情報)
        if(!empty($request->file_name)){
            //複数POSTされることを考慮してループ処理
            foreach( $request->file_name as $file ){
                $param_attachments = [
                    'task_id' => $max_id,
                    'file_name' => $file,
                    'created_at' => Carbon::now('Asia/Tokyo'),
                    'updated_at' => Carbon::now('Asia/Tokyo'),
                ];
                if(!empty($file)){
                    DB::table('attachments')->insert($param_attachments);
                }
            }
        }

        //ファイル保存
        if(!empty($request->file('file'))){
            $files = $request->file('file');
            foreach ($files as $file) {
                $fileName = $file->getClientOriginalName();
                $path = $file->storeAs('public/'.$max_id, $fileName);
            }
        }

        return view('task_registered');
    }


    /**
     * データの更新処理
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        //楽観的排他制御
        $exclusive_check = Task::find($request->id);
        if($exclusive_check->updated_at > $request->updated_at) {
            // エラーメッセージを付与して入力画面へ戻す
            \App::abort(501, 'Something bad happened');
        }

        //DB接続確認
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            \App::abort(500, 'Something bad happened');
        }

        //POSTされた要素をセッションに保存        
        $request->session()->put('id_regist', $request->id);
        $request->session()->put('title_regist', $request->title);
        $request->session()->put('end_expected_date_regist', $request->date);
        $request->session()->put('achievement_flg_regist', $request->achivment);
        $request->session()->put('detail_regist', $request->detail);
        $request->session()->put('updated_at', $request->updated_at);
        
        //バリデーションルール
        $rules = [
            'title'        => ['required'],
            'date'         => ['required','regex:|\d{4}\/\d{1,2}\/\d{1,2}|'],
            'detail'       => ['required','max: 400'],
        ];

        //バリデーションメッセージ
        $messages = [
            'title.required'            => '必須入力です',
            'date.required'            => '必須入力です',
            'detail.required'            => '必須入力です',
            
            'date.regex' => 'YYYY/MM/DDの形で入力して下さい(From側)',
            'detail.max'   => '400文字まで入力できます',
        ];

        //バリデーター
        $validator = Validator::make($request->all(),$rules,$messages);

        if($validator->fails()){
            return redirect('/taskregistration/'.$request->id.'/validation_error')
                            ->withErrors($validator)
                            ->withInput();
        }

        //POSTされた要素とID情報でDBに更新処理(タスク情報)        
        $param_tasks = [
            'title' => $request->title,
            'end_expected_date' => $request->date,
            'achievement_flg' => $request->achivment,
            'detail' => $request->detail,
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->where('id', $request->id)
                          ->update($param_tasks);

        //POSTされた要素とID情報でDBに更新処理(添付ファイル情報)                                
        if(!empty($request->file_name)){
            //複数POSTされることを考慮してループ処理
            foreach( $request->file_name as $file ){
                $param_attachments = [
                    'task_id' => $request->id,
                    'file_name' => $file,
                    'created_at' => Carbon::now('Asia/Tokyo'),
                    'updated_at' => Carbon::now('Asia/Tokyo'),
                ];
                if(!empty($file)){
                    DB::table('attachments')->insert($param_attachments);
                }
            }
        }

        //ファイル保存
        if(!empty($request->file('file'))){
            $files = $request->file('file');

            foreach ($files as $file) {
                $fileName = $file->getClientOriginalName();
                $path = $file->storeAs('public/'.$request->id, $fileName);
            }
        }
        
        return view('task_registered');
    }

}
