<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->path() == 'tasklist'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'numeric|nullable',
            'from_date' => 'date_format:"Y\/m\/d"|nullable',
            'to_date' => 'date_format:"Y\/m\/d"|nullable',
        ];
    }

    public function messages()
    {
        return [
            'id.numeric' => 'idは半角数字で入力して下さい',
            'from_date.date_format' => 'YYYY/MM/DDの形で入力して下さい(From側)',
            'to_date.date_format' => 'YYYY/MM/DDの形で入力して下さい(To側)',
        ];
    }
}
