<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    /**
     * 特定タスクの全添付ファイル取得
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }


}
