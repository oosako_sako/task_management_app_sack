<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class AttachmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $param = [
            'task_id' => '1',
            'file_name' => 'test01.png',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('attachments')->insert($param);
        
        $param = [
            'task_id' => '1',
            'file_name' => 'test02.png',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('attachments')->insert($param);
        
        $param = [
            'task_id' => '1',
            'file_name' => 'test03.png',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('attachments')->insert($param);

        $param = [
            'task_id' => '2',
            'file_name' => 'test04.png',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('attachments')->insert($param);

        $param = [
            'task_id' => '2',
            'file_name' => 'test05.png',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('attachments')->insert($param);

        $param = [
            'task_id' => '3',
            'file_name' => 'test06.png',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('attachments')->insert($param);

    }
}
