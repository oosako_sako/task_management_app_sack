<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * ※データの作成日は自動的に登録されないのは何故？
     *
     * @return void
     */
    public function run()
    {
        $param = [
            'title' => 'test1',
            'end_expected_date' => '2018-01-01',
            'achievement_flg' => '2',
            'detail' => 'test_detail_1',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->insert($param);
        
        $param = [
            'title' => 'test2',
            'end_expected_date' => '2018-02-01',
            'achievement_flg' => '2',
            'detail' => 'test_detail_2',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->insert($param);
        
        $param = [
            'title' => 'test3',
            'end_expected_date' => '2018-03-01',
            'achievement_flg' => '2',
            'detail' => 'test_detail_3',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->insert($param);

        $param = [
            'title' => 'テスト',
            'end_expected_date' => '2018-04-01',
            'achievement_flg' => '1',
            'detail' => '日本語のテスト',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->insert($param);
        
        $param = [
            'title' => 'テスト１',
            'end_expected_date' => '2018-05-01',
            'achievement_flg' => '1',
            'detail' => '日本語のテスト1',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->insert($param);
        
        $param = [
            'title' => 'テスト２',
            'end_expected_date' => '2018-06-01',
            'achievement_flg' => '1',
            'detail' => '日本語のテスト2',
            'created_at' => Carbon::now('Asia/Tokyo'),
            'updated_at' => Carbon::now('Asia/Tokyo'),
        ];
        DB::table('tasks')->insert($param);
    }
}
