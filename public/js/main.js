
const storage = localStorage;

var counter = 1;

if(storage.getItem('test') !== null){

    counter = parseInt(storage.getItem('test'));
    
    for( var i = 1; i < counter; i++){
    var linefeed = document.createElement('br');

    var text_insertion = document.createElement('span'),
    label = document.createTextNode('添付ファイル：');

    var span = document.createElement('span');

    var input_text = document.createElement('input');
    var button = document.createElement('button');
    var button_label = document.createTextNode('参照');
    var input_file = document.createElement('input');


    linefeed.id   = 'linefeed'   + i;
    text_insertion.id = 'text_insertion' + i;
    span.id  = 'span' + i;


    input_text.id  = "filename" + i;
    input_text.type  = "text";
    input_text.name  = "file_name[]";   
    input_text.readOnly = true;

    button.id = i ;
    button.classList.add("btn");
    button.type = "button";

    input_file.id = "file_button" + i ;
    input_file.type = "file";
    input_file.name = "file[]";
    input_file.multiple = true;
    input_file.style.display="none";

    input_text.value = "";
    document.getElementById('attach_file').appendChild(linefeed);
    document.getElementById('attach_file').appendChild(text_insertion).appendChild(label);
    document.getElementById('attach_file').appendChild(span).appendChild(input_text);
    document.getElementById('attach_file').appendChild(span).appendChild(button).appendChild(button_label);
    document.getElementById('attach_file').appendChild(span).appendChild(input_file);

    }

}


$(document).on('click', '.btn' , function(){
    var id =  $(this).attr("id");
    $("#file_button" + id).click();
    $(document).on('change', '#file_button'+ id, function() { 
        $('#filename' + id).val($(this).val().replace(/^.*\\/, ""));
    });
});

var add_tag = function(){
    var linefeed = document.createElement('br');

    var text_insertion = document.createElement('span'),
        label = document.createTextNode('添付ファイル：');

    var span = document.createElement('span');

    var input_text = document.createElement('input');
    var button = document.createElement('button');
    var button_label = document.createTextNode('参照');
    var input_file = document.createElement('input');


    linefeed.id   = 'linefeed'   + counter;
    text_insertion.id = 'text_insertion' + counter;
    span.id  = 'span' + counter;


    input_text.id  = "filename" + counter;
    input_text.type  = "text";
    input_text.name  = "file_name[]";   
    input_text.readOnly = true;

    button.id = counter ;
    button.classList.add("btn");
    button.type = "button";

    input_file.id = "file_button" + counter ;
    input_file.type = "file";
    input_file.name = "file[]";
    input_file.multiple = true;
    input_file.style.display="none";

    input_text.value = "";
    document.getElementById('attach_file').appendChild(linefeed);
    document.getElementById('attach_file').appendChild(text_insertion).appendChild(label);
    document.getElementById('attach_file').appendChild(span).appendChild(input_text);
    document.getElementById('attach_file').appendChild(span).appendChild(button).appendChild(button_label);
    document.getElementById('attach_file').appendChild(span).appendChild(input_file);

    counter += parseInt(1);

    storage.setItem('test', counter);
}

var remove_tag = function(){
    if(counter > 1){
        counter -= parseInt(1);
        var linefeed = document.getElementById('linefeed' + counter);
        var text_insertion = document.getElementById('text_insertion' + counter);
        var span = document.getElementById('span' + counter);
        linefeed.parentNode.removeChild(linefeed);
        text_insertion.parentNode.removeChild(text_insertion);
        span.parentNode.removeChild(span);
    }
    storage.setItem('test', counter);

}

document.getElementById('add').addEventListener('click', add_tag);
document.getElementById('remove').addEventListener('click', remove_tag);

(function(){
    'use strict';
    //厳格モード
    //従来受け入れられた一部のミスがエラーになる(開発段階では推奨)

    var open = document.getElementById('open');
    var close = document.getElementById('close');
    var modal = document.getElementById('modal');
    var mask = document.getElementById('mask');

    open.addEventListener('click', function() {
        modal.className = '';
        mask.className = '';

        var modal_file;

        var form = document.getElementById("form");

        var title = document.getElementById("title").value;
        var date = document.getElementById("datepicker1").value;
        var status = document.getElementsByName("achivment");
        var detail = document.getElementById("detail").value;
        var file = document.getElementsByName("file_name[]")

        if(status[0].checked){
            var status_msg = '達成済み';
        }else{
            var status_msg = '未達成';
        }

        document.getElementById('modal_title').innerHTML = title;
        document.getElementById('modal_date').innerHTML = date;
        document.getElementById("modal_status").innerHTML = status_msg;
        document.getElementById("modal_detail").innerHTML = detail;

        for(var i = 0; i < file.length ; i++ ){
            document.getElementById("modal_file").innerHTML += file[i].value + "<br>";
        }


    });

    close.addEventListener('click', function() {
        modal.className = 'hidden';
        mask.className = 'hidden';
        document.getElementById("modal_file").innerHTML = "";
    });

    mask.addEventListener('click', function() {
        document.getElementById("modal_file").innerHTML = "";
        close.click();
    });

}());
