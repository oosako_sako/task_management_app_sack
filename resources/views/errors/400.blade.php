@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>400 Bad Request</p>
    <p>リクエストにエラーがあります</p>
    <p>このレスポンスは、構文が無効であるためサーバーがリクエストを理解できないことを示します。</p>
</div>
@endsection