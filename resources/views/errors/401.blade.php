@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>401 Unauthorized</p>
    <p>認証に失敗しました</p>
    <p>リクエストされたリソースを得るために認証が必要です。</p>
</div>
@endsection