@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>403 Forbidden</p>
    <p>あなたにはアクセス権がありません</p>
    <p>クライアントがコンテンツへのアクセス権を持たず、サーバーが適切な応答への返信を拒否していることを示します。</p>
</div>
@endsection