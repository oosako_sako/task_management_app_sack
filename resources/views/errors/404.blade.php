@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>404 Not Found</p>
    <p>該当アドレスのページを見つける事ができませんでした</p>
    <p>サーバーは要求されたリソースを見つけることができなかったことを示します。 URLのタイプミス、もしくはページが移動または削除された可能性があります。 トップページに戻るか、もう一度検索してください。</p>
</div>
@endsection