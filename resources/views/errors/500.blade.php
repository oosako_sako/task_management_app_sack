@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>500 Internal Server Error</p>
    <p>サーバー内部でエラーが発生しました</p>
    <p>プログラムに文法エラーがあったり、設定に誤りがあった場合などに返されます。管理者へ連絡してください。</p>
</div>
@endsection