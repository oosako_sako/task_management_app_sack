@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>501 Exclusion Error</p>
    <p>排他エラーが発生しました</p>
    <p>重複して更新処理が行われた可能性があります。</p>
</div>
@endsection