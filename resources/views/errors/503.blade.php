@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>503 Service Unavailable</p>
    <p>このページへは事情によりアクセスできません</p>
    <p>サービスが一時的に過負荷やメンテナンスで使用不可能な状態です。</p>
</div>
@endsection