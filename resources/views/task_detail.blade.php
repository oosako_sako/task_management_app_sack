@extends('layouts.task_app')

@section('content')
<div class="task-box">
    <div>
        <span>ID：　　　　</span>
        <span class="list01">{{ $items_task[0]->id }}</span>
        <br>
        <br>
        <span>タイトル：　</span>
        <span class="list02">{{ $items_task[0]->title }}</span>
        <br>
        <br>
        <span>達成予定日：</span>
        <span class="list03">{{ str_replace('-', '/', $items_task[0]->end_expected_date) }}</span>
        <br>
        <br>
        <span>ステータス：</span>
        <span class="list04">
            @if ( $items_task[0]->achievement_flg === '1' )
                <span>達成済み</span>
            @else
                <span>未達成</span>
            @endif
        </span>
        <br>
    </div>
    <br>
    <div>
        <div class="btn_left">
            <a href="{{ url('/tasklist') }}" class="square_btn">戻る</a>
            <div class="btn_light">
                <a href="{{ url('/taskregistration/'.$items_task[0]->id) }}" class="square_btn">編集する</a>
            </div>
        </div>
    </div>
</div>
<br>
<div class="detail-box">
    <span>詳細内容：</span>
    <br>
    <span class="list05">{{ $items_task[0]->detail }}</span>
</div>
<br>
<div class="attach-file-box">
    <span>添付ファイル：</span>
    <br>
    <ul>
        @foreach ($items_attachment as $item)
            <li><a href="{{ url('/taskdetail/'.$items_task[0]->id.'/'.$item->file_name) }}" class="list06">{{ $item->file_name }}</a></li>
            <br>
        @endforeach
    </ul>
</div>
@endsection