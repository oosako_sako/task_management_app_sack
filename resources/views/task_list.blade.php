@extends('layouts.task_app')

@section('content')
<div class="search-box">
    <form action="{{ url('/tasklist') }}" method="POST">
    {{ csrf_field() }}
        <span>ID検索：　　　　</span>
        <input type="text" name="id" value="{{ session('id') }}">
        <br>
        <p class="error">
            @if ($errors->has('id'))
                {{$errors->first('id')}}
            @endif
        </p>

        <span>タイトル検索：　</span>
        <input type="text" name="title" value="{{ session('title') }}">
        <br>

        <p class="error"></p>

        <span>達成予定日検索：</span>
        <input type="text" name="from_date" id="datepicker1" autocomplete="off" value="{{ session('from_date') }}">
        <span>～</span>
        <input type="text" name="to_date"   id="datepicker2" autocomplete="off" value="{{ session('to_date') }}">
        <br>

        <p class="error">
            @if ($errors->has('from_date'))
                {{$errors->first('from_date')}}
            @endif
        </p>

        <p class="error">
            @if ($errors->has('to_date'))
                {{$errors->first('to_date')}}
            @endif
        </p>              

        <span>添付ファイル：　</span>
        <input type="radio" name="file" value="1" @if(session('file') == "1") checked @endif><span>あり　</span>
        <input type="radio" name="file" value="2" @if(session('file') == "2") checked @endif><span>なし　</span>
        <input type="radio" name="file" value=""  @if(session('file') == "") checked @endif><span>すべて　</span>
        <br>
        <br>

        <input type="checkbox" name="achivment" value="1" @if(session('achivment') == "1") checked @endif>
        <span>達成済みのタスクも検索に含める</span>
        <br>
        <br>

        <input type="hidden" name="sort"        value="{{ session('sort') }}">
        <input type="hidden" name="asc_desc"    value="{{ session('asc_desc') }}">
        <input type="hidden" name="post_check"  value="1">
        <div>
            <div class="btn_center">
                <input type="submit" value="検索" class="square_btn">
                <div class="btn_light">
                    <a href="{{ url('/taskregistration')}}" class="square_btn">新規登録</a>
                </div>
            </div>
        </div>
    </form>
</div>
<br>
<div class="list-box">
    <table class="table_all">
        <thead>
            <tr>
                <th class="table_title01" >
                <span>ID</span>
                <form action="{{ url('/tasklist') }}" method="POST" style="display: inline-block; _display: inline;">
                {{ csrf_field() }}
                    <input type="hidden" name="id"                 value="{{ session('id') }}">
                    <input type="hidden" name="title"              value="{{ session('title') }}">
                    <input type="hidden" name="from_date"          value="{{ session('from_date') }}">
                    <input type="hidden" name="to_date"            value="{{ session('to_date') }}">
                    <input type="hidden" name="file"               value="{{ session('file') }}">
                    <input type="hidden" name="achivment"          value="{{ session('achivment') }}">
                    <input type="hidden" name="sort"               value="id" >
                    <input type="hidden" name="asc_desc"           value="asc" >
                    <input type="hidden" name="post_check"         value="1">
                    <input type="submit" value="▲" class="btn-clear">
                </form>
                <form action="{{ url('/tasklist') }}" method="POST" style="display: inline-block; _display: inline;">
                {{ csrf_field() }}
                    <input type="hidden" name="id"                 value="{{ session('id') }}">
                    <input type="hidden" name="title"              value="{{ session('title') }}">
                    <input type="hidden" name="from_date"          value="{{ session('from_date') }}">
                    <input type="hidden" name="to_date"            value="{{ session('to_date') }}">
                    <input type="hidden" name="file"               value="{{ session('file') }}">
                    <input type="hidden" name="achivment"          value="{{ session('achivment') }}">
                    <input type="hidden" name="sort"               value="id" >
                    <input type="hidden" name="asc_desc"           value="desc" >
                    <input type="hidden" name="post_check"         value="1">
                    <input type="submit" value="▼" class="btn-clear">
                </form>
                </th>
                <th class="table_title02" >
                <span>タイトル</span>
                <form action="{{ url('/tasklist') }}" method="POST" style="display: inline-block; _display: inline;">
                {{ csrf_field() }}
                    <input type="hidden" name="id"                 value="{{ session('id') }}">
                    <input type="hidden" name="title"              value="{{ session('title') }}">
                    <input type="hidden" name="from_date"          value="{{ session('from_date') }}">
                    <input type="hidden" name="to_date"            value="{{ session('to_date') }}">
                    <input type="hidden" name="file"               value="{{ session('file') }}">
                    <input type="hidden" name="achivment"          value="{{ session('achivment') }}">
                    <input type="hidden" name="sort"               value="title" >
                    <input type="hidden" name="asc_desc"           value="asc" >
                    <input type="hidden" name="post_check"         value="1">
                    <input type="submit" value="▲" class="btn-clear">
                </form>
                <form action="{{ url('/tasklist') }}" method="POST" style="display: inline-block; _display: inline;">
                {{ csrf_field() }}
                    <input type="hidden" name="id"                 value="{{ session('id') }}">
                    <input type="hidden" name="title"              value="{{ session('title') }}">
                    <input type="hidden" name="from_date"          value="{{ session('from_date') }}">
                    <input type="hidden" name="to_date"            value="{{ session('to_date') }}">
                    <input type="hidden" name="file"               value="{{ session('file') }}">
                    <input type="hidden" name="achivment"          value="{{ session('achivment') }}">
                    <input type="hidden" name="sort"               value="title" >
                    <input type="hidden" name="asc_desc"           value="desc" >
                    <input type="hidden" name="post_check"         value="1">
                    <input type="submit" value="▼" class="btn-clear">
                </form>

                </th>
                <th class="table_title03" >
                <span>達成状況</span>
                <form action="{{ url('/tasklist') }}" method="POST" style="display: inline-block; _display: inline;">
                {{ csrf_field() }}
                    <input type="hidden" name="id"                 value="{{ session('id') }}">
                    <input type="hidden" name="title"              value="{{ session('title') }}">
                    <input type="hidden" name="from_date"          value="{{ session('from_date') }}">
                    <input type="hidden" name="to_date"            value="{{ session('to_date') }}">
                    <input type="hidden" name="file"               value="{{ session('file') }}">
                    <input type="hidden" name="achivment"          value="{{ session('achivment') }}">
                    <input type="hidden" name="sort"               value="achievement_flg" >
                    <input type="hidden" name="asc_desc"           value="asc" >
                    <input type="hidden" name="post_check"         value="1">
                    <input type="submit" value="▲" class="btn-clear">
                </form>
                <form action="{{ url('/tasklist') }}" method="POST" style="display: inline-block; _display: inline;">
                {{ csrf_field() }}
                    <input type="hidden" name="id"                 value="{{ session('id') }}">
                    <input type="hidden" name="title"              value="{{ session('title') }}">
                    <input type="hidden" name="from_date"          value="{{ session('from_date') }}">
                    <input type="hidden" name="to_date"            value="{{ session('to_date') }}">
                    <input type="hidden" name="file"               value="{{ session('file') }}">
                    <input type="hidden" name="achivment"          value="{{ session('achivment') }}">
                    <input type="hidden" name="sort"               value="achievement_flg" >
                    <input type="hidden" name="asc_desc"           value="desc" >
                    <input type="hidden" name="post_check"         value="1">
                    <input type="submit" value="▼" class="btn-clear">
                </form>
                </th>
                <th class="table_title04"></th>
            </tr>
        </thead>
        @if (!empty($items)) 
            @foreach ($items as $item)
                <tr>
                    <td class="table_list00">{{ $item->id }}</td>
                    <td class="table_list02">{{ $item->title }}</td>
                    <td class="table_list01">
                        @if ( $item->achievement_flg === '1' )
                            <span>OK</span>
                        @endif
                    </td>
                    <td class="table_list03">
                    <a href="{{ url('/taskdetail/'.$item->id) }}"class="table_link">詳細情報</a>
                    </td>
                </tr>
            @endforeach
        @endif
        <tbody>
        </tbody>
    </table>
</div>
@endsection

