@extends('layouts.task_app')

@section('content')
<div class="input-box" style="text-align: center;">
    <p>タスクを正常に登録しました</p>
    <div class="btn_modal">
        <a href="{{ url('/tasklist')}}" class="square_btn">タスク一覧に戻る</a>
    </div>
</div>
@endsection