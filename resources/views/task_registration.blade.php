@extends('layouts.task_app')

@section('content')
<div class="input-box">
    <form id="form" action=" @if(!empty($id)) {{ url('/taskregistration/'.$id) }} @else {{ url('/taskregistration')}}  @endif" enctype="multipart/form-data" method="POST"> 
    {{ csrf_field() }}
        <input type="hidden" name="updated_at" value="{{ session('updated_at') }}">
        <span>タイトル：　　</span><!--
        --><input id="title" type="text" name="title" value="{{ session('title_regist') }}">
        <br>
        <p class="error">
            @if ($errors->has('title'))
                {{$errors->first('title')}}
            @endif
        </p>

        <span>達成予定日：　</span><!--
        --><input type="text" name="date" id="datepicker1" autocomplete="off" value="{{ session('end_expected_date_regist') }}">
        <br>
        <p class="error">
            @if ($errors->has('date'))
                {{$errors->first('date')}}
            @endif
        </p>            

        <span>ステータス：　</span><!--
     --><input type="radio" name="achivment" value="1" @if(session('achievement_flg_regist') == "1") checked @endif><span>達成済み　</span>
        <input type="radio" name="achivment" value="2" @if(session('achievement_flg_regist') == "2" || session('achievement_flg_regist') == "" ) checked @endif><span>未達成　</span>
        <br>
        <p class="error">
            @if ($errors->has('achivment'))
                {{$errors->first('achivment')}}
            @endif
        </p>

        <span>詳細内容：　　</span><!--
        --><textarea id="detail" name="detail" rows="4" cols="40" style="resize: none; vertical-align:top;">{{ session('detail_regist') }}</textarea>
        <br>
        <p class="error">
            @if ($errors->has('detail'))
                {{$errors->first('detail')}}
            @endif
        </p>

        <div id="attach_file">
            @if ( !empty($items_attachment[0]) )
                @foreach ( $items_attachment as $item )
                    @if ( $loop->first )
                        <div style="float: right;">
                            <button type="button" id="add" class="square_btn">入力エリア追加</button>
                            <div>
                                <button type="button" id="remove" class="square_btn">入力エリア削除</button>
                            </div>
                        </div>
                    @endif
                    <span>添付ファイル：</span><!--
                 --><input  id="filename"          type="text" name="file_name[]" disabled value="{{ $item->file_name }}" ><!--
                 --><button id="select_file_button"type="button" disabled >参照</button><!--
                 --><input  id="file_button"       type="file" style="display: none;" name="file[]" multiple="multiple" disabled >
                    @if ( !$loop->last )
                    <br>
                    @endif
                @endforeach
            @else
                <span>添付ファイル：</span><!--
             --><input  id="filename_origin"    type="text"   name="file_name[]" value="" readonly><!--
             --><button id="_origin"            type="button" class="btn" >参照</button><!--
             --><input  id="file_button_origin" type="file"   name="file[]" multiple="multiple" style="display: none;"  >
                <div style="float: right;">
                    <button type="button" id="add" class="square_btn">入力エリア追加</button>
                    <div>
                        <button type="button" id="remove" class="square_btn">入力エリア削除</button>
                    </div>
                </div>
            @endif
        </div>
        <br>
        <br>


        <div>
            <div class="btn_center_conf">
                <input type="button" value="確認" class="square_btn" id="open">
                <div class="btn_left_back_to_detail">
                @if(!empty($items_attachment))
                            <a href="{{ url('/taskdetail/'.$id) }}" class="square_btn">戻る</a>
                @else
                            <a href="{{ url('/tasklist')}}" class="square_btn">戻る</a>
                @endif
                </div>
            </div>
        </div>
        <div id="modal" class="hidden">
            <span>タイトル：</span>
            <p id="modal_title"></p>
            <span>達成予定日：</span>
            <p id="modal_date"></p>
            <span>ステータス：</span>
            <p id="modal_status"></p>
            <span>詳細内容：</span>
            <p id="modal_detail"></p>
            <span>添付ファイル：</span>
            <ul id="modal_file"></ul>

            <div class="btn_modal">
                <input type="submit" value="登録" class="square_btn">
                <div id="close">
                    キャンセル
                </div>
            </div>
        </div> 
    </form>
</div>


<div id="mask" class="hidden"></div>
<script src="{{ asset('js/main.js') }}"></script>
@endsection