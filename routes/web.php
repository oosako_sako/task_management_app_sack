<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'auth.very_basic'], function() {

    Route::get('/', 'TaskListController@index');
    Route::get('/tasklist', 'TaskListController@show');
    Route::get('/taskdetail/{id}', 'TaskDetailController@show_id')->where('id','[0-9]+');
    Route::get('/taskdetail/{id}/{file_name}', 'TaskDetailController@file_download')->where('id','[0-9]+');
    Route::get('/taskregistration', 'TaskRegistrationController@show');
    Route::get('/taskregistration/{id}', 'TaskRegistrationController@show_id')->where('id','[0-9]+');
    Route::get('/taskregistration/validation_error', 'TaskRegistrationController@show_valid');
    Route::get('/taskregistration/{id}/validation_error', 'TaskRegistrationController@show_id_valid')->where('id','[0-9]+');

    Route::post('/tasklist', 'TaskListController@search');
    Route::post('/taskregistration', 'TaskRegistrationController@create');
    Route::post('/taskregistration/{id}', 'TaskRegistrationController@update')->where('id','[0-9]+');

});

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('error/{code}', function ($code) {
//     abort($code);
//   });